#include <iostream>
#include <chrono>
#include <stack>
#include <sstream>
#include <list>
#include <vector>
#include <random>
#include <algorithm>

using namespace std;

class A
{
public:
    int content[10000];
    string toString(){ return std::to_string(content[0]);}

    A()
    {
        for(int i=0;i<10000;++i) content[i] = 1+i;

    }


};

class B
{
public:
    int content[15000];
    string toString(){ return to_string(content[0]);}
    B()
    {
        for(int i=0;i<15000;++i) content[i] = i+1;
    }

};

int main(int argc, char ** argv) {

    std::chrono::steady_clock::time_point started;

    int startTimingIteration = 5;

    auto start = std::chrono::high_resolution_clock::now();

    int  maxIterations = 10;


    vector<A*> vectorA;
    vector<B*> vectorB;
    int numberOfObjects = 10000;



    for(int j=0; j<maxIterations; ++j)
    {
        if(j == startTimingIteration)
            started = std::chrono::steady_clock::now();
        
        for(int i=0;i<numberOfObjects;++i)
        {
            vectorA.push_back(new A());
        }



        vector<int> objectsToDelete;
        std::default_random_engine r;
        std::uniform_int_distribution<> d(0,numberOfObjects-1);
        for(int i=0; i< numberOfObjects/2; i++)
        {
            int idx = d(r);
            if(find(objectsToDelete.begin(),objectsToDelete.end(),idx) != objectsToDelete.end())
            {
                i--; continue;
            }
            objectsToDelete.push_back(idx);
        }

        std::sort(objectsToDelete.begin(), objectsToDelete.end());
        std::reverse(objectsToDelete.begin(), objectsToDelete.end());

        for(int i=0; i<objectsToDelete.size(); i++)
        {
            int deleteHere = objectsToDelete[i];
            delete  vectorA[deleteHere];

        }
        for(int i=0; i<objectsToDelete.size(); i++)
        {
            int deleteHere = objectsToDelete[i];
            vectorA.erase(vectorA.begin()  + deleteHere);

        }



        for(int i=0;i<vectorA.size();i++)
        {
            vectorB.push_back(new B());
        }


        for(int i=0;i<vectorA.size();i++) {
            cout << i << flush;
            cout << ": A =" << vectorA[i]->toString() ;
            cout << " B =" << vectorB[i]->toString();
            cout << endl;
        }


        for(int i=0;i<vectorA.size();i++) {
            delete vectorA[i];
            delete vectorB[i];
        }

        vector<A*>::iterator it1 = vectorA.begin();
        vector<B*>::iterator it2 = vectorB.begin();
        while(it1 != vectorA.end())
            it1 =vectorA.erase(it1);
        while(it2 != vectorB.end())
            it2 =vectorB.erase(it2);


    }

    auto end = std::chrono::high_resolution_clock::now();
    int msec = std::chrono::duration_cast<std::chrono::milliseconds> (end - start).count();
    int duration = ((float) msec);
    cout << "Duration : " <<  duration << " msec" <<  std::endl ;
    cout << "Average Duration : " <<  duration / maxIterations<< " msec" <<  std::endl ;


    return 1;
}