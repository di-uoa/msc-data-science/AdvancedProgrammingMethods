import javax.lang.model.element.Element;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Ask2 {

    public static void main (String [] args){

        int numberofthreads = 30;
        LinkedList mylist = new LinkedList();
        ElementsofList element1 = new ElementsofList();
        ElementsofList element2 = new ElementsofList();
        ElementsofList element3 = new ElementsofList();

        element1.data = "A";
        element2.data = "B";
        element3.data = "C";

        mylist.prepend(element1);
        mylist.prepend(element2);
        mylist.prepend(element3);

        ArrayList <PrependThread> insert = new ArrayList<PrependThread>();
        ArrayList <PopThread> pop = new ArrayList<PopThread>();
        ArrayList <HeadThread> header = new ArrayList<HeadThread>();

        Executor exec = Executors.newFixedThreadPool(numberofthreads);

        for (int i=0; i<numberofthreads; i++) {

            insert.add(new PrependThread());
            insert.get(i).name = "prepend" + i;
            insert.get(i).setList(mylist);

            pop.add(new PopThread());
            pop.get(i).name = "pop" + i;
            pop.get(i).setList(mylist);

            header.add(new HeadThread());
            header.get(i).name = "head" + i;
            header.get(i).setList(mylist);

        }

        for (int i=0; i<numberofthreads; i++) {
            exec.execute(insert.get(i));
            exec.execute(pop.get(i));
            exec.execute(header.get(i));


        }

    }
}
