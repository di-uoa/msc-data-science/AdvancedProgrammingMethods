package ObserverExample;

import java.util.ArrayList;
import java.util.Observable;

public class BookDataBase extends Observable {

    ArrayList <String> booksdata;

    //Arxikopoihsh tou ArrayList

    public void InitializeDataBase() {

        booksdata = new ArrayList <String> ();

        booksdata.add("The Conquest of Bread");
        booksdata.add("Pride and Prejudice");
        booksdata.add("The Stranger");
        booksdata.add("The Brothers Karamazov");
        booksdata.add("Mutual Aid: A Factor of Evolution");
        booksdata.add("Nineteen Eighty-Four");
        booksdata.add("Jitterbug Perfume");
        booksdata.add("Villa Incognito");
        booksdata.add("God and the State");
    }

    //Prosthiki bibliwn. Otan klei8ei auth h sunarthsh, tote eidopoiountai oloi oi observers oti egine update

    public void addBooks(String newdata) {
        booksdata.add(newdata);
        setChanged();
        notifyObservers(booksdata);
    }

    public void removeBook (String deletedata) {
        for(int i = 0; i<booksdata.size(); i++){
            if(booksdata.get(i) == deletedata) {
                booksdata.remove(booksdata.get(i));
                setChanged();
                notifyObservers(booksdata);
            }
        }
    }


}
