package ObserverExample;

import java.util.ArrayList;
import java.util.Observer;
import java.util.Observable;

public class BookController extends Observable implements Observer {

    //O controller eidopoieitai mesw ths update oti egine kapoia allagh sth lista twn bibliwn. Einai tautoxrona kai observer kai observable, afou eidpoiei
    //ton user gia opoiadhpote allagh

    @Override
    public void update(Observable listofbooks, Object updatedlist) {

        ArrayList<String> myList =   (ArrayList<String>) ((ArrayList<String>)updatedlist).clone();
        for(int i=0; i<myList.size(); ++ i) {
            String namestring = Integer.toString(i+1);
            String newdata = namestring + ". " + myList.get(i);
            myList.set (i, newdata);

            }
        setChanged();
        notifyObservers(myList);
    }
}
