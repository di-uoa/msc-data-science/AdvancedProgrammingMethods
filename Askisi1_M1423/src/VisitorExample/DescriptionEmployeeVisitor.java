package VisitorExample;

public class DescriptionEmployeeVisitor extends EmployeeVisitor {

    void visit (HRemployee hre) {
        System.out.println("The HR employee " +hre.employee + " works in the position " + hre.position + ". The HR Department is responsible for payroll services, training, hygiene, security and evaluation of employees.");
    }

    void visit (Accountingemployee ace) {
        System.out.println("The employee " + ace.employee + " works in the position " +ace.position + " in the Accounting Department. This Department is responsible for paying company's vendors and receiving money from its clients.");
    }

    void visit (ITemployee ite){
        System.out.println("The employee " + ite.employee + " works in the position " + ite.position + " in the IT Department. This department is responsible for technical support.");
    }
}
