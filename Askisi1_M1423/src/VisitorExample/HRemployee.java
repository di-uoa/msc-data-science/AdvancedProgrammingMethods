package VisitorExample;

public class HRemployee extends Employee {

    int bonus_HR;

    protected HRemployee (String employee_name, int total_years, String manager_name, String position_name){
        super(employee_name, total_years, manager_name, position_name);
        bonus_HR = 50;
    }

    void accept (EmployeeVisitor v) {
       v.visit(this);
   }
}
