package VisitorExample;

public class PrintManagerVisitor extends EmployeeVisitor {
    void visit (HRemployee hre) {
        System.out.println("The HR Manager is: " + hre.manager);
    }

    void visit (Accountingemployee ace) {
        System.out.println("The Accounting Manager is: " + ace.manager);
    }

    void visit (ITemployee ite) {
        System.out.println("The IT Manager is: " + ite.manager);
    }

}
