package VisitorExample;

abstract class EmployeeVisitor {
    abstract void visit (HRemployee hre);
    abstract void visit (ITemployee ite);
    abstract void visit (Accountingemployee ace);
}
