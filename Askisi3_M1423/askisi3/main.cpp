#include <iostream>
#include <cstdio>

using namespace std;

class C1 {
public:
    int field1;
    virtual void meth1(){
        cout << "Method 1 in class C1" << endl;
    }
    virtual void func1() {
        cout << "Function1 in class C1" << endl;
    }
};

class C2 {
public:
    int field2;
    virtual void meth2(){
        cout << "Method 2 in class C2" << endl;
    }
    virtual void func2(){
        cout << "Function 2 in class C2" << endl;
    }
};
//class C3 inherits classes C1 and C2, with primary class C1
class C3: public C1, public C2{
public:
    int field3;
    //overriding meth1 and meth2
    void meth1(){
        cout << "Method 1 in class C3" << endl;
    }
    void meth2(){
        cout << "Method 2 in class C3" << endl;
    }
    void func3(){
        cout << "Function 3 in class C3" << endl;
    }

};

int main() {

    C1 *c1 = new C1();
    C2 *c2 = new C2();
    C3 *c3 = new C3();

    c1->field1 = 10;
    c2->field2 = 20;
    c3->field3 = 30;

    //Prints the offset from the beginning of the v-table for virtual methods and for func3 which is not virtual we obtain the
    //real address where the code of the method is stored
    cout << "Printing c1 object layout" <<endl;
    printf("%p\n",&C1::field1);
    printf("%p\n",&C1::meth1 );
    printf("%p\n",&C1::func1 );

    cout << "Printing c2 object layout" <<endl;
    printf("%p\n",&C2::field2);
    printf("%p\n",&C2::meth2 );
    printf("%p\n",&C2::func2 );

    cout << "Printing c3 object layout" <<endl;
    printf("%p\n",&C3::field1);
    printf("%p\n",&C3::field2);
    printf("%p\n",&C3::field3);
    printf("%p\n",&C3::meth1 );
    printf("%p\n",&C3::meth2 );
    printf("%p\n",&C3::func3 );

    void*** pointer1 = (void ***)c1;
    void*** pointer2 = (void ***)c2;
    void*** pointer3 = (void ***)c3;

    //Print the address of the object and the address of v-table. We move the pointer in order to obtain the address of all the
    //functions in the v-table
    cout<< "Printing v-table for class C1"<< endl;
    printf("%p\n",pointer1[0]);
    printf("%p\n",pointer1[0][0]);
    printf("%p\n",pointer1[0][1]);

    cout<< "Printing v-table for class C2"<< endl;
    printf("%p\n",pointer2[0]);
    printf("%p\n",pointer2[0][0]);
    printf("%p\n",pointer2[0][1]);

    cout<< "Printing v-table for class C3"<< endl;
    printf("%p\n",pointer3[0]);
    printf("%p\n",pointer3[0][0]);
    printf("%p\n",pointer3[0][1]);
    printf("%p\n",pointer3[0][2]);
    printf("%p\n",pointer3[0][3]);

    return 0;
}